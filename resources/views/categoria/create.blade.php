@extends('layouts.plantillabase')

@section('contenido')
<h2>CREAR CATEGORIAS</h2>

<form action="/categorias" method="POST">
    @csrf
<div class="mb-3">
    <label for="" class="from-label">Nombre</label>
    <input id="nombre" name="nombre" type="text" class="form-control" tabindex="1">
  </div>
  <div class="mb-3">
    <label for="" class="from-label">Descripcion</label>
    <input id="descripcion" name="descripcion" type="text" class="form-control" tabindex="2">
  </div>
  <a href="/categorias" class="btn btn-secondary" tabindex="4">Cancelar</a>
  <button type="submit" class="btn btn-primary" tabindex="3">Guardar</button>
</from>
@endsection