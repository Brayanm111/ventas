@extends('layouts.plantillabase')

@section('contenido')
<h2>EDITAR REGISTROS</h2>

<form action="/articulos/{{$articulo->id_articulos}}" method="POST">
    @csrf    
    @method('PUT')
<div class="mb-3">
    <label for="" class="from-label">Codigo</label>
    <input id="id_categoria" name="id_categoria" type="text" class="form-control" value="{{$articulo->id_categoria}}">
  </div>
<div class="mb-3">
    <label for="" class="from-label">Nombre</label>
    <input id="nombre" name="nombre_articulo" type="text" class="form-control"  value="{{$articulo->nombre_articulo}}">
  </div>
  <div class="mb-3">
    <label for="" class="from-label">Precio</label>
    <input id="precio" name="precio_articulo" type="number" step="any" class="form-control" value="{{$articulo->precio_articulo}}">
  </div>
  <div class="mb-3">
    <label for="" class="from-label">Cantidad</label>
    <input id="cantidad" name="cantidad" type="number" class="form-control" value="{{$articulo->cantidad}}">
  </div>
  <div class="mb-3">
    <label for="" class="from-label">Descripcion</label>
    <input id="descripcion" name="descripcion" type="text" class="form-control"value="{{$articulo->descripcion}}">
  </div>
  <a href="/articulos" class="btn btn-secondary">Cancelar</a>
  <button type="submit" class="btn btn-primary">Guardar</button>
</from>
@endsection